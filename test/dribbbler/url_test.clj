(ns dribbbler.url-test
  (:require [clojure.test :refer [deftest is testing]]
            [clojure.spec.test :as stest]
            [clojure.spec.gen :as sgen]
            [clojure.test.check.clojure-test :as tcheck]
            [clojure.test.check.properties :as prop]
            [dribbbler.url :as url]
            [clojure.spec :as s]))

(deftest pattern-test
  (testing "empty pattern, empty string, invalid pattern"
    (let [pat-str ""]
      (is (= nil (url/pattern pat-str))))

    (let [pat-str "abc"]
      (is (= nil (url/pattern pat-str)))))

  (testing "success parsing"
    (let [pat-str "host(twitter.com); path(?user/status/?id);"
          res {:host "twitter.com" :path [:user "status" :id] :qps nil}]
      (is (= res (url/pattern pat-str))))

    (let [pat-str "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);"
          res {:host "dribbble.com" :path ["shots" :id] :qps {"offset" :offset}}]
      (is (= res (url/pattern pat-str))))

    (let [pat-str "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);"
          res {:host "dribbble.com" :path ["shots" :id] :qps {"offset" :offset "list" :type}}]
      (is (= res (url/pattern pat-str))))))

(deftest recognize-test

  (testing "host and path"
    (let [pat (url/pattern "host(twitter.com); path(?user/status/?id);")
         url "http://twitter.com/bradfitz/status/562360748727611392"
         res [[:user "bradfitz"] [:id 562360748727611392]]]
     (is (= res (url/recognize pat url)))))

  (testing "host, path and query params"
    (let [pat (url/pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);")
          url "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1"
          res [[:id "1905065-Travel-Icons-pack"] [:offset 1]]]
      (is (= res (url/recognize pat url)))))

  (testing "wrong host"
    (let [pat (url/pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);")
          url "https://twitter.com/shots/1905065-Travel-Icons-pack?list=users&offset=1"
          res nil]
      (is (= res (url/recognize pat url)))))

  (testing "missing query param"
    (let [pat (url/pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);")
         url "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users"
         res nil]
     (is (= res (url/recognize pat url)))))

  (testing "host, path and several query params"
    (let [pat (url/pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);")
          url "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1"
          res [[:id "1905065-Travel-Icons-pack"] [:offset 1] [:type "users"]]]
      (is (= res (url/recognize pat url)))))

  (testing "host, path and several query params with literal"
    (let [pat (url/pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=users);")
          url "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1"
          res [[:id "1905065-Travel-Icons-pack"] [:offset 1]]]
      (is (= res (url/recognize pat url)))))

  (testing "host, path and several query params with wrong literal"
    (let [pat (url/pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=disaster);")
          url "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1"
          res nil]
      (is (= res (url/recognize pat url))))))