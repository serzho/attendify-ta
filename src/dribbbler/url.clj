(ns dribbbler.url
  (:require [clojure.spec :as s]
            [clojure.spec.test :as stest]
            [clojure.test :refer [is]]
            [clojure.string :as string]
            [clojure.pprint :as pp]))

(defn- t [arg]
  (pp/pprint arg)
  arg)

(defn- bindable? [str]
  (string/starts-with? str "?"))

(defn- create-bind [str]
  (keyword (subs str 1)))

(def ^:private bind? keyword?)

(defn- coerce [value]
  (try
    (Long/parseLong value)
    (catch NumberFormatException _
      value)))

(defn- parse-path [value]
  (->> (string/split value #"/")
       (map (fn [p]
              (if (bindable? p)
                (create-bind p)
                p)))))

(defn- parse-qps [values]
  (reduce (fn [acc qp]
            (let [[name value] (string/split qp #"=")
                  bind (if (bindable? value)
                              (create-bind value)
                              value)]
              (assoc acc
                name bind)))
          {} values))

(defn- extract-part [part-str]
  (nth (re-find (re-matcher #"(\w+)\(([^\(|\)]+)\)" part-str)) 2))

(defn- match-host [pat host]
  (= pat host))

(defn- match-path [pat path]
  (when (= (count pat) (count path))
    (->> (map vector pat path)
         (reduce (fn [acc [pat-part path-part]]
                   (cond
                     (bind? pat-part)       (conj acc [pat-part (coerce path-part)])
                     (= pat-part path-part) acc
                     :else                  (reduced nil)))
                 []))))


(defn- match-qps [pat qps]
  (reduce (fn [acc [pat-name pat-value]]
            (let [value (get qps pat-name)]
              (cond
                (and value (bind? pat-value)) (conj acc [pat-value (coerce value)])
                (= value pat-value)           acc
                :else                         (reduced nil)))
            )
          [] pat))

(defn- split-qps [qps]
  (when (seq qps)
    (->> (string/split qps #"&")
         (map #(string/split % #"="))
         (into {}))))

(defn- split [str]
  (let [[rest qps-str] (string/split str #"\?")
        qps (split-qps qps-str)
        [_proto _ host & path] (string/split rest #"/")]
    [host path qps]))

(s/def ::pattern-host string?)
(s/def ::pattern-path (s/cat :path-parts (s/* (s/or :part string? :bind keyword?))))
(s/def ::pattern-qps (s/map-of string? (s/or :const string? :bind keyword?)))
(s/def ::pattern (s/nilable (s/keys :opt [::pattern-host ::pattern-path ::pattern-qps])))

(defn pattern [pat-str]
  (when pat-str
    (let [[host path & qps :as parts] (map extract-part (string/split pat-str #"; ?"))]
      (when-not (some nil? parts)
        {:host host
         :path (when path (parse-path path))
         :qps  (when qps (parse-qps qps))}))))

(s/fdef pattern
  :args (s/cat :pat-str (s/and string? seq))
  :ret ::pattern)

;; TODO: write fn spec

(defn recognize [pat str]
  (when pat
    (let [[host path qps] (split str)
          host-matching (match-host (:host pat) host)
          path-matching (match-path (:path pat) path)
          qps-matching (match-qps (:qps pat) qps)]
      (when (and host-matching path-matching qps-matching)
        (concat path-matching qps-matching)))))

(s/fdef recognize
  :args (s/cat :pattern ::pattern :str string?)
  :ret (s/coll-of (s/tuple keyword? (s/or :s string? :n number?))))