(ns dribbbler.top10-followers
  (:require [org.httpkit.client :as http]
            [cheshire.core :refer [parse-string]]
            [clojure.string :as string]
            [clojure.core.cache :as cache]
            [clojure.pprint :as pp]))

(def ^:private base "https://api.dribbble.com/v1")
(def ^:private access-token "454fc2f91109fa0d5e943eab86759bbc3a781155f77269a9485237f745daeb61")

(defonce ^:private cache (atom (cache/ttl-cache-factory {} :ttl (* 30 60 1000))))

(defn- req* [url]
  (if (cache/has? @cache url)
    (do (swap! cache cache/hit url)
        (let [p (promise)]
          (deliver p (get @cache url))
          p))
    (http/get url {:query-params {"access_token" access-token}}
              (fn [{:keys [status] :as r}]
                (when (not= status 429)
                  (swap! cache cache/miss url r))
                r))))



(defn- url [& path-parts]
  (string/join "/" (cons base path-parts)))

(defn- parse [res]
  (parse-string (:body res)))

(defn- reqs [urls]
  (map (comp parse deref req*) urls))

(defn top10
  "Returns top 10 likers of shots of followers of provided username.
   May return stale results due to extensive caching."
  [user-name]
  (let [followers-url (->> (reqs [(url "users" user-name)])
                           (map #(get % "followers_url")))
        shots-urls (->> (reqs followers-url)
                       (apply concat)
                       (map #(get-in % ["follower" "shots_url"])))
        likes-urls (->> (reqs shots-urls)
                       (apply concat)
                       (map #(get % "likes_url")))
        likes (->> (reqs likes-urls)
                   (apply concat)
                   (map #(get-in % ["user" "username"])))]
    (->> likes
         (remove nil?)
         (group-by identity)
         (map (fn [[n ns]] [n (count ns)]))
         (sort-by second (comp - compare))
         (take 10))))

(comment
  (top10 "jack")
  (top10 "john")

  )